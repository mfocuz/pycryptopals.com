#!/bin/python
import struct
import binascii
import base64

base64map = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
             'V', 'W', 'X', 'Y', 'Z',
             'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
             'v', 'w', 'x', 'y', 'z',
             '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/']


def base64_encode(inputData):
    inputBinary = binascii.a2b_uu(inputData)
    return inputBinary

def base64_decode(input):

    return 0

if __name__ == '__main__':
    # input string to encode/decode
    hexToEncode = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    # This is correct base64 encoded string for testing
    correctBase64Encode = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
    try:
        b64encoded = base64_encode(hexToEncode.decode('hex'))
    except:
        print binascii.Error
